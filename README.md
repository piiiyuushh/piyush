# *Configuration of Ansible for servers*
## Defining size of log files of nginx
#### We can define log size of log file of nginx, for that we use option which exists in Unix and linux with name of logrotation in this file we can define what is maximum size and of a single log file.
---

### I have created two node with name of node1 and node2.
![1](/uploads/3e8d744d13efb455559c93adbfb11492/1.png)
## Inventory file
![2](/uploads/7f69ed74f3eab9bb59c8ce22f2094776/2.png)

## then We Install Nginx on worker 1

![3](/uploads/8eb1986ed350a9d2ad5a5fe0ca84af39/3.png)

## ![nginx log file]
![4_nginx_logfile](/uploads/88bcf457b588cdff1a62662ed405b86f/4_nginx_logfile.png)

## logrotation nginx

![5_logrotation](/uploads/5a3d4c16ac2bbefef9ca77153d1a8204/5_logrotation.png)
## CURl all host server

![curl_ping](/uploads/d96ee22043869e5fa08a806fa089598c/curl_ping.png)
### Apache2 configuration
![6_apache_ports_conf](/uploads/0b961fa3791e2dbff6db62003258325e/6_apache_ports_conf.png)
---
## Output
![8_playbook_output](/uploads/5dc17654b84ffad9d8d91e588f07755c/8_playbook_output.png)
### when I ran playbook I get this output

![nginx_script](/uploads/9fa3a791e5fcb7920a2c2eb603c51caa/nginx_script.png)
---
### Output from web browser


![output](/uploads/83f8b7ddaf7a119c3a94e2fbec302372/output.png)
### Output of nginx 

![output_nginx](/uploads/69ffeb1f5584aaf648d9d9f7f022bdb7/output_nginx.png)
